from modeltranslation.admin import TranslationAdmin
from django.contrib import admin
from .models import Category, Information, Work, Cv, Tag




class WorksAdmin(TranslationAdmin):
    pass
admin.site.register(Work, WorksAdmin)

class InformationAdmin(TranslationAdmin):
    pass
admin.site.register(Information, InformationAdmin)

class CategoryAdmin(TranslationAdmin):
    pass
admin.site.register(Category, CategoryAdmin)

class CvAdmin(TranslationAdmin):
    pass
admin.site.register(Cv, CvAdmin)

admin.site.register(Tag)
