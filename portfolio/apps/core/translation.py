from modeltranslation.translator import translator, TranslationOptions
from .models import Work, Information, Category, Cv

class WorksTranslationOptions(TranslationOptions):
    fields = ('title', 'content', 'used_tools')

translator.register(Work, WorksTranslationOptions)

class InfomationTranslationOptions(TranslationOptions):
    fields = ('title', 'content')

translator.register(Information, InfomationTranslationOptions)

class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)

translator.register(Category, CategoryTranslationOptions)

class CvTranslationOptions(TranslationOptions):
    fields = ('title', 'link_url')

translator.register(Cv, CvTranslationOptions)
